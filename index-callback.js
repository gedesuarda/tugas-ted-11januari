const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

// const { Book, User } = require('./models')
const { Book,User,Todo } = require('./models')

app.use(express.json())


//function GET all ........................................................................................................

function getAllBook(callback) {
    Book.findAll().then((data) => {
        return callback(null, data)
      })
      .catch((error) => {
        return callback(error, null)
      })
 }

 function getAllUser(callback) {
    User.findAll().then((data) => {
        return callback(null, data)
      })
      .catch((error) => {
        return callback(error, null)
      })
 }
 
 function getAllTodo(callback) {
    Todo.findAll().then((data) => {
        return callback(null, data)
      })
      .catch((error) => {
        return callback(error, null)
      })
 }

//endpoint GET all...................................................................................................

app.get("/books", (req, res) => {
    getAllBook((error, data) => {
        if(error) {
            res.json(error)
          }
      
        res.status(200).json({
            status: true,
            message: "All Books retrieved",
            data: data,
        })
    })
})

app.get("/users", (req, res) => {
    getAllUser((error, data) => {
        if(error) {
            res.json(error)
          }
      
        res.status(200).json({
            status: true,
            message: "All Users retrieved",
            data: data,
        })
    })
})

app.get("/todos", (req, res) => {
    getAllTodo((error, data) => {
        if(error) {
            res.json(error)
          }
      
        res.status(200).json({
            status: true,
            message: "All Todos retrieved",
            data: data,
        })
    })
})


//function GET id ........................................................................................................

function getBook(id, callback) {
    Book.findByPk(id)
      .then((data) => {
        return callback(null, data);
      })
      .catch((error) => {
        return callback(error, null);
    });
}

function getUser(id, callback) {
    User.findByPk(id)
      .then((data) => {
        return callback(null, data);
      })
      .catch((error) => {
        return callback(error, null);
    });
}

function getTodo(id, callback) {
    Todo.findByPk(id)
      .then((data) => {
        return callback(null, data);
      })
      .catch((error) => {
        return callback(error, null);
    });
}


//endpoint GET id.............................................................................................

app.get("/book/:id", (req, res) => {
    const { id } = req.params;
    getBook(id, (error, data) => {
      if (error) {
        res.json(error);
      }
  
      res.status(200).json({
        status: true,
        message: `Book with id ${id} retrieved`,
        data: data,
      })
    })
})

app.get("/user/:id", (req, res) => {
    const { id } = req.params;
    getUser(id, (error, data) => {
      if (error) {
        res.json(error);
      }
  
      res.status(200).json({
        status: true,
        message: `Book with id ${id} retrieved`,
        data: data,
      })
    })
})

app.get("/todo/:id", (req, res) => {
    const { id } = req.params;
    getTodo(id, (error, data) => {
      if (error) {
        res.json(error);
      }
  
      res.status(200).json({
        status: true,
        message: `Book with id ${id} retrieved`,
        data: data,
      })
    })
})


//Function POST ..........................................................................................................

function createBook(data, callback) {
    Book.create(data)
      .then((data) => {
        return callback(null, data);
      })
      .catch((error) => {
        return callback(error, null);
      });


function createUser(data, callback) {
    User.create(data)
      .then((data) => {
        return callback(null, data);
      })
      .catch((error) => {
        return callback(error, null);
});


function createTodo(data, callback) {
    Todo.create(data)
      .then((data) => {
        return callback(null, data);
      })
      .catch((error) => {
        return callback(error, null);
});

//endpoint POST .........................................................................................................

app.post("/books", (req, res) => {
    const data = {
      title: req.body.title,
      author: req.body.author,
    };
  
    createBook(data, (error, bookInserted) => {
      if (error) {
        res.json(error);
      }
  
      res.status(201).json({
        status: true,
        message: "Books created!",
        data: bookInserted,
      })
    })
})

app.post("/users", (req, res) => {
    const data = {
      title: req.body.title,
      author: req.body.author,
    };
  
    createUser(data, (error, userInserted) => {
      if (error) {
        res.json(error);
      }
  
      res.status(201).json({
        status: true,
        message: "Users created!",
        data: userInserted,
      })
    })
})

app.post("/todos", (req, res) => {
    const data = {
      title: req.body.title,
      author: req.body.author,
    };
  
    createTodo(data, (error, todoInserted) => {
      if (error) {
        res.json(error);
      }
  
      res.status(201).json({
        status: true,
        message: "Books created!",
        data: todoInserted,
      })
    })
})

//function PUT.......................................................................................................... 

function updateBook(id, data, callback) {
    Book.update(data, { where: { id } })
      .then((updatedBook) => {
        return callback(null, updatedBook);
      })
      .catch((error) => {
        return callback(error, null);
    });
}

function updateUser(id, data, callback) {
    User.update(data, { where: { id } })
      .then((updatedUser) => {
        return callback(null, updatedUser);
      })
      .catch((error) => {
        return callback(error, null);
    });
}

function updateTodo(id, data, callback) {
    Todo.update(data, { where: { id } })
      .then((updatedTodo) => {
        return callback(null, updatedTodo);
      })
      .catch((error) => {
        return callback(error, null);
    });
}

//endpoint PUT ..........................................................................................................

app.put("/book/:id", (req, res) => {
    const { id } = req.params;
    getBook(id, (error, data) => {
      if (error) {
        res.json(error);
      }
  
      const newBook = {
        title: req.body.title || book.title,
        author: req.body.author || book.author,
      };
  
      updateBook(id, newBook, (error, updatedBook) => {
        if (error) {
          res.json(error);
        }
  
        res.status(200).json({
          status: true,
          message: `Book with id ${id} updated!`,
        })
      })
    })
})

app.put("/user/:id", (req, res) => {
    const { id } = req.params;
    getBook(id, (error, data) => {
      if (error) {
        res.json(error);
      }
  
      const newBook = {
        title: req.body.title || book.title,
        author: req.body.author || book.author,
      };
  
     updateBook(id, newBook, (error, updatedUser) => {
        if (error) {
          res.json(error);
        }
  
        res.status(200).json({
          status: true,
          message: `User with id ${id} updated!`,
        })
      })
    })
})

app.put("/todo/:id", (req, res) => {
    const { id } = req.params;
    getBook(id, (error, data) => {
      if (error) {
        res.json(error);
      }
  
      const newBook = {
        title: req.body.title || book.title,
        author: req.body.author || book.author,
      };
  
     updateBook(id, newBook, (error, updatedUser) => {
        if (error) {
          res.json(error);
        }
  
        res.status(200).json({
          status: true,
          message: `Todo with id ${id} updated!`,
        })
      })
    })
})



//function DELETE ..........................................................................................................

function deleteBook(id, callback) {
    Book.destroy({
      where: {
        id,
      },
    })
    .then((data) => {
        return callback(null, data);
    })
    .catch((error) => {
        return callback(error, null);
    });
  
}

function deleteUser(id, callback) {
    User.destroy({
      where: {
        id,
      },
    })
      .then((data) => {
        return callback(null, data);
      })
      .catch((error) => {
        return callback(error, null);
      });
  
}

function deleteTodo(id, callback) {
    Todo.destroy({
      where: {
        id,
      },
    })
      .then((todo) => {
        return callback(null, todo);
      })
      .catch((error) => {
        return callback(error, null);
      });
}


//endpoint DELETE ........................................................................................................

app.delete("/books/:id", (req, res) => {
    const { id } = req.params;
    deleteBook(id, (error, data) => {
      if (error) {
        res.json(error);
      }
  
      res.status(201).json({
        status: true,
        message: `Books with id ${id} deleted!`,
      })
    })
})

app.delete("/users/:id", (req, res) => {
    const { id } = req.params;
    deleteUser(id, (error, data) => {
      if (error) {
        res.json(error);
      }
  
      res.status(201).json({
        status: true,
        message: `User with id ${id} deleted!`,
      })
    })
})

app.delete("/books/:id", (req, res) => {
    const { id } = req.params;
    deleteTodo(id, (error, data) => {
      if (error) {
        res.json(error);
      }
  
      res.status(201).json({
        status: true,
        message: `Todo with id ${id} deleted!`,
      })
    })
})

app.listen(port, () => console.log(`Listening on port ${port}!`))