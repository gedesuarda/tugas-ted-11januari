const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

// const { Book, User } = require('./models')
const { Book, User, Todo } = require('./models')

app.use(express.json())

//endpoint book
app.get('/books', (req, res) => { //endpoint all dibuat plural ('s) untuk menghindari error
Book.findAll().then(data => {
  res.status(200).json({
    status: true,
    message: 'Books retrieved!',
    data: data
  })
})
})

app.get('/book/:id', (req, res) => { //endpoint book dibuat singular, bukan plural (tanpa pake tanda 's)
  Book.findByPk(req.params.id).then(data => {
    if(!data){
      res.status(200).json({
        status: false,
        message: `Book with ID ${req.params.id} not found`,
        data: data
      })
    }else{
      res.status(200).json({
        status: true,
        message: `Book with ID ${req.params.id} retrieved!`,
        data: data
      })
    }
  })
})

app.post('/books', (req, res) => {
  Book.create({
    title: req.body.title,
    author: req.body.author
  }).then(data => {
    res.status(201).json({
      status: true,
      message: 'Books created!',
      data: data
    })
  })
})

app.put('/book/:id', (req, res) => {
  Book.findByPk(req.params.id).then(data => {
    if(!data){
      res.status(200).json({
        status: false,
        message: `Book with ID ${req.params.id} not found!`,
        data: data 
      })
    }else{
        data.update({
        title: req.body.title,
        author: req.body.author
      }, {
        where: {
          id: req.params.id
        }
      }).then(() => {
        res.status(200).json({
          status: true,
          message: `Book with ID ${req.params.id} updated!`,
          data: data 
        })
      })
    }
  })
})

app.delete('/book/:id', (req, res) => {
  Todo.findByPk(req.params.id).then(data => {
    if (!data){
      res.status(200).json({
        status: false,
        message: `Book with ID ${req.params.id} not found`,
        data: data
      })
    }else{
      Todo.destroy({
        where: {
          id: req.params.id
        }
      }).then(() => {
        res.status(200).json({
            status: true,
            message: `Book with ID ${req.params.id} deleted!`,
            data: data
          })
        }
      )
    }

  })
})
    

//endpoint todo

app.get('/todos', (req, res) => { //endpoint all dibuat plural ('s) untuk menghindari error
Todo.findAll().then(data => {
  res.status(200).json({
    status: true,
    message: 'Todos retrieved!',
    data: data
  })
})
})

app.get('/todo/:id', (req, res) => { //endpoint book dibuat singular, bukan plural (tanpa pake tanda 's)
Todo.findByPk(req.params.id).then(data => {
  if(!data){res.status(200).json({
    status: false,
    message: `Todo with ID ${req.params.id} not found!`,
    data: data
  })
  }else{
    res.status(200).json({
      status: true,
      message: `Todo with ID ${req.params.id} retrieved!`,
      data: data
    })
  }
    
  })
})

app.post('/todos', (req, res) => {
  Todo.create({
    user: req.body.user,
    todo: req.body.todo
  }).then(data => {
    res.status(201).json({
      status: true,
      message: 'Todos created!',
      data: data
    })
  })
})

app.put('/todo/:id', (req, res) => {
  Todo.findByPk(req.params.id).then(data => {
    if(!data){
      res.status(200).json({
        status: false,
        message: `Todo with ID ${req.params.id} not found!`,
        data: data 
      })
    }else{
        data.update({
        user: req.body.user,
        todo: req.body.todo
      }, {
        where: {
          id: req.params.id
        }
      }).then(() => {
        res.status(200).json({
          status: true,
          message: `Todo with ID ${req.params.id} updated!`,
          data: data 
        })
      })
    }
  })
})

app.delete('/todo/:id', (req, res) => {
  Todo.findByPk(req.params.id).then(data => {
    if(!data){
      res.status(200).json({
        status: false,
        message: `Todo with ID ${req.params.id} not found!`,
        data: data
      })
    }else{
      res.status(200).json({
        status: true,
        message: `Todo with ID ${req.params.id} deleted!`,
        data: data
      })
    }
  })
})

//end point user 

app.get('/users', (req, res) => { //endpoint all dibuat plural ('s) untuk menghindari error
User.findAll().then(data => {
  res.status(200).json({
    status: true,
    message: 'Users retrieved!',
    data: data
  })
})
})

app.get('/user/:id', (req, res) => { //endpoint book dibuat singular, bukan plural (tanpa pake tanda 's)
User.findByPk(req.params.id).then(data => {
  if(!data){res.status(200).json({
    status: false,
    message: `User with ID ${req.params.id} not found!`,
    data: data
  })
  }else{
    res.status(200).json({
      status: true,
      message: `User with ID ${req.params.id} retrieved!`,
      data: data
    })
  }
    
  })
})

app.post('/users', (req, res) => {
  User.create({
    user: req.body.user,
    password: req.body.password
  }).then(data => {
    res.status(201).json({
      status: true,
      message: 'Users created!',
      data: data
    })
  })
})

app.put('/user/:id', (req, res) => {
  User.findByPk(req.params.id).then(data => {
    if(!data){
      res.status(200).json({
        status: false,
        message: `User with ID ${req.params.id} not found!`,
        data: data 
      })
    }else{
        data.update({
        user: req.body.user,
        password: req.body.password
      }, {
        where: {
          id: req.params.id
        }
      }).then(() => {
        res.status(200).json({
          status: true,
          message: `User with ID ${req.params.id} updated!`,
          data: data 
        })
      })
    }
  })
})

app.delete('/user/:id', (req, res) => {
  User.findByPk(req.params.id).then(data => {
    if(!data){
      res.status(200).json({
        status: false,
        message: `User with ID ${req.params.id} not found!`,
        data: data
      })
    }else{
      res.status(200).json({
        status: true,
        message: `User with ID ${req.params.id} deleted!`,
        data: data
      })
    }
  })
})

app.listen(port, () => console.log(`Listening on port ${port}!`))
