const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

// const { Book, User } = require('./models')
const { Book,User,Todo } = require('./models')

app.use(express.json())

//endpoint books
app.get('/books', async(req, res) => { //endpoint all dibuat plural ('s) untuk menghindari error
    let data = await Book.findAll()
    if(data) {
        res.status(200).json({
            status: true,
            message: 'Books retrieved!',
            data: data
        })
    }
})

app.get('/book/:id', async(req, res) => { //endpoint book dibuat singular, bukan plural (tanpa pake tanda 's)
    let data = await Book.findByPk(req.params.id)
    if(!data) {
        res.status(200).json({
            status: false,
            message: `Book with ID ${req.params.id} not found!`,
            data: data
        })
     }else {
        res.status(200).json({
            status: true,
            message: `Book with ID ${req.params.id} retrieved!`,
            data: data
        })
    }
})

app.post('/books', async(req, res) => {
        let data = await Book.create({
            title: req.body.title,
            author: req.body.author
        })
        if(!data){
            res.status(201).json({
                status: false,
                message: 'Books creation failed!',
                data: data
            })
        }else{
            res.status(201).json({
                status: true,
                message: 'Books created!!',
                data: data
            })
        }
})

app.put('/book/:id', async(req, res) => {
    let data = await Book.findByPk(req.params.id)
    if(!data) {
        res.status(200).json({
            status: false,
            message: `Book with ID ${req.params.id} not found!`,
            data: data
        })
     }else {
        data.update({
            user: req.body.user,
            todo: req.body.todo
          }, {
            where: {
              id: req.params.id
            }
          });

        res.status(200).json({
            status: true,
            message: `Book with ID ${req.params.id} updated!`,
            data: data
        })
    }
})

app.delete('/book/:id', async(req, res) => {
    let data = await Book.findByPk(req.params.id)
    if(!data) {
        res.status(200).json({
            status: false,
            message: `Book with ID ${req.params.id} not found!`,
            data: data
        })
     }else {
        data.destroy({
            where: {
              id: req.params.id
            }
          });

        res.status(200).json({
            status: true,
            message: `Book with ID ${req.params.id} deleted!`,
            data: data
        })
    }
})

// endpoint todo

app.get('/todos', async(req, res) => { //endpoint all dibuat plural ('s) untuk menghindari error
    let data = await Todo.findAll()
    if(data) {
        res.status(200).json({
            status: true,
            message: 'Todos retrieved!',
            data: data
        })
    }
})

app.get('/todo/:id', async(req, res) => { //endpoint book dibuat singular, bukan plural (tanpa pake tanda 's)
    let data = await Todo.findByPk(req.params.id)
    if(!data) {
        res.status(200).json({
            status: false,
            message: `Todo with ID ${req.params.id} not found!`,
            data: data
        })
     }else {
        res.status(200).json({
            status: true,
            message: `Todo with ID ${req.params.id} retrieved!`,
            data: data
        })
    }
})

app.post('/todos', async(req, res) => {
        let data = await Todo.create({
            user: req.body.user,
            todo: req.body.todo
        })
        if(!data){
            res.status(201).json({
                status: false,
                message: 'Todos creation failed!',
                data: data
            })
        }else{
            res.status(201).json({
                status: true,
                message: 'Todos created!!',
                data: data
            })
        }
})

app.put('/todo/:id', async(req, res) => {
    let data = await Todo.findByPk(req.params.id)
    if(!data) {
        res.status(200).json({
            status: false,
            message: `Todo with ID ${req.params.id} not found!`,
            data: data
        })
     }else {
        data.update({
            user: req.body.user,
            todo: req.body.todo
          }, {
            where: {
              id: req.params.id
            }
          });

        res.status(200).json({
            status: true,
            message: `Todo with ID ${req.params.id} updated!`,
            data: data
        })
    }
})

app.delete('/todo/:id', async(req, res) => {
    let data = await Todo.findByPk(req.params.id)
    if(!data) {
        res.status(200).json({
            status: false,
            message: `Todo with ID ${req.params.id} not found!`,
            data: data
        })
     }else {
        data.destroy({
            where: {
              id: req.params.id
            }
          });

        res.status(200).json({
            status: true,
            message: `Todo with ID ${req.params.id} deleted!`,
            data: data
        })
    }
})

// endpoint user

app.get('/users', async(req, res) => { //endpoint all dibuat plural ('s) untuk menghindari error
    let data = await User.findAll()
    if(data) {
        res.status(200).json({
            status: true,
            message: 'Users retrieved!',
            data: data
        })
    }
})

app.get('/user/:id', async(req, res) => { //endpoint book dibuat singular, bukan plural (tanpa pake tanda 's)
    let data = await User.findByPk(req.params.id)
    if(!data) {
        res.status(200).json({
            status: false,
            message: `User with ID ${req.params.id} not found!`,
            data: data
        })
     }else {
        res.status(200).json({
            status: true,
            message: `User with ID ${req.params.id} retrieved!`,
            data: data
        })
    }
})

app.post('/users', async(req, res) => {
        let data = await User.create({
            user: req.body.user,
            password: req.body.password
        })
        if(!data){
            res.status(201).json({
                status: false,
                message: 'Users creation failed!',
                data: data
            })
        }else{
            res.status(201).json({
                status: true,
                message: 'Users created!!',
                data: data
            })
        }
})

app.put('/user/:id', async(req, res) => {
    let data = await User.findByPk(req.params.id)
    if(!data) {
        res.status(200).json({
            status: false,
            message: `User with ID ${req.params.id} not found!`,
            data: data
        })
     }else {
        data.update({
            user: req.body.user,
            password: req.body.password
          }, {
            where: {
              id: req.params.id
            }
          });

        res.status(200).json({
            status: true,
            message: `User with ID ${req.params.id} updated!`,
            data: data
        })
    }
})

app.delete('/user/:id', async(req, res) => {
    let data = await User.findByPk(req.params.id)
    if(!data) {
        res.status(200).json({
            status: false,
            message: `User with ID ${req.params.id} not found!`,
            data: data
        })
     }else {
        data.destroy({
            where: {
              id: req.params.id
            }
          });

        res.status(200).json({
            status: true,
            message: `User with ID ${req.params.id} deleted!`,
            data: data
        })
    }
})

app.listen(port, () => console.log(`Listening on port ${port}!`))